#pragma once

typedef void(*voidFuncPtr)(void);
class RTCWrapper {
public:
	friend class RTCTimeManager;
	virtual void standbyMode() = 0;
	virtual void init(uint8_t h, uint8_t mi, uint8_t s, uint8_t d, uint8_t mo, uint8_t y) = 0;
private:


	virtual uint32_t getEpoch() = 0;
	virtual uint32_t getY2kEpoch() = 0;
	virtual void setEpoch(uint32_t ts) = 0;
	virtual void setY2kEpoch(uint32_t ts) = 0;
	virtual void setAlarmEpoch(uint32_t ts) = 0;

	virtual uint8_t getSeconds() = 0;
	virtual uint8_t getMinutes() = 0;
	virtual uint8_t getHours() = 0;

	virtual void setSeconds(uint8_t seconds) = 0;
	virtual void setMinutes(uint8_t minutes) = 0;
	virtual void setHours(uint8_t hours) = 0;
	virtual void setTime(uint8_t hours, uint8_t minutes, uint8_t seconds) = 0;

	virtual void setDay(uint8_t day) = 0;
	virtual void setMonth(uint8_t month) = 0;
	virtual void setYear(uint8_t year) = 0;
	virtual void setDate(uint8_t day, uint8_t month, uint8_t year) = 0;

	virtual uint8_t getDay() = 0;
	virtual uint8_t getMonth() = 0;
	virtual uint8_t getYear() = 0;


	virtual void enableAlarm(uint8_t mask) = 0;
	virtual void disableAlarm() = 0;

	virtual void attachInterrupt(voidFuncPtr callback) = 0;
	virtual void detachInterrupt() = 0;

	virtual void setAlarmSeconds(uint8_t s) = 0;
	virtual void setAlarmMinutes(uint8_t m) = 0;
	virtual void setAlarmHours(uint8_t h) = 0;
	virtual void setAlarmTime(uint8_t h, uint8_t m, uint8_t s) = 0;
	virtual void setAlarmDay(uint8_t d) = 0;
	virtual void setAlarmMonth(uint8_t m) = 0;
	virtual void setAlarmYear(uint8_t y) = 0;
	virtual void setAlarmDate(uint8_t d, uint8_t m, uint8_t y) = 0;

	virtual uint8_t getAlarmSeconds(uint8_t s) = 0;
	virtual uint8_t getAlarmMinutes(uint8_t m) = 0;
	virtual uint8_t getAlarmHours(uint8_t h) = 0;
	virtual uint8_t getAlarmDay(uint8_t d) = 0;
	virtual uint8_t getAlarmMonth(uint8_t m) = 0;
	virtual uint8_t getAlarmYear(uint8_t y) = 0;

};
