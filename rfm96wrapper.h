#ifndef RFM69WRAPPER_H
#define RFM69WRAPPER_H

#include "remotetodrone.h"
#include "RH_RF95.h"
#include "RHReliableDatagram.h"

#define RFM96_CS    8
#define RFM96_INT   3
#define RFM96_RST   4
#define RFM96_FREQ	433.0
#define FRAME_LENGTH 66

//retransmission parameters to determine after how many time or how many retries the transmissions is deemed to have failed 
#define TIMEOUT 50
#define RETRIES 1

/****************************************************************************/
/*  @brief this class serves as a kind of wrapper to the rfm69 radio module	*/ 
/*	as used with de radiohead library										*/
/****************************************************************************/
class RFM96Wrapper : public RemoteToDrone
{
public:  
	RFM96Wrapper(); 
	virtual ~RFM96Wrapper();
	
	//RemoteToDrone interface
	virtual bool send(uint8_t * data, uint8_t length) override;
	virtual void receive(uint8_t* data) override;
	virtual void init() override;
	
	
	uint16_t getPacketNumber() const { return packetNumber; }
	void setPacketNumber(uint16_t val) { packetNumber = val; } 

private:
	RH_RF95 _theDriver; 
	uint16_t packetNumber; 
	uint8_t* ptLength; 
	uint8_t* ptTimeout; 
};

#endif // RFM69WRAPPER_H
