#include "RTCZeroWrapper.hpp"

inline uint8_t RTCZeroWrapper::getSeconds()
{
	return rtc.getSeconds();
}

inline uint8_t RTCZeroWrapper::getMinutes()
{
	return rtc.getMinutes();
}

inline uint8_t RTCZeroWrapper::getHours()
{
	return rtc.getHours();
}

void RTCZeroWrapper::standbyMode()
{
	rtc.standbyMode();
}

void RTCZeroWrapper::init(uint8_t h, uint8_t mi, uint8_t s, uint8_t d, uint8_t mo, uint8_t y)
{
	rtc.begin();
	rtc.setTime(h, mi, s);
	rtc.setDate(d, mo, y);
}

void RTCZeroWrapper::disableAlarm()
{
	rtc.disableAlarm();
}

void RTCZeroWrapper::enableAlarm(uint8_t mask)
{
	rtc.enableAlarm((RTCZero::Alarm_Match)mask);
}

void RTCZeroWrapper::setAlarmSeconds(uint8_t s)
{
	rtc.setAlarmSeconds(s);
}

void RTCZeroWrapper::setAlarmMinutes(uint8_t m)
{
	rtc.setAlarmMinutes(m);
}

void RTCZeroWrapper::setAlarmHours(uint8_t h)
{
	rtc.setAlarmHours(h);
}

void RTCZeroWrapper::setAlarmDay(uint8_t d)
{
	rtc.setAlarmDay(d);
}

void RTCZeroWrapper::setAlarmMonth(uint8_t m)
{
	rtc.setAlarmMonth(m);
}

void RTCZeroWrapper::setAlarmYear(uint8_t y)
{
	rtc.setAlarmYear(y);
}

uint8_t RTCZeroWrapper::getAlarmSeconds(uint8_t s)
{
	return rtc.getAlarmSeconds();
}

uint8_t RTCZeroWrapper::getAlarmMinutes(uint8_t m)
{
	return rtc.getAlarmMinutes();
}

uint8_t RTCZeroWrapper::getAlarmHours(uint8_t h)
{
	return rtc.getAlarmHours();
}

uint8_t RTCZeroWrapper::getAlarmDay(uint8_t d)
{
	return rtc.getAlarmDay();
}

uint8_t RTCZeroWrapper::getAlarmMonth(uint8_t m)
{
	return rtc.getAlarmMonth();
}

uint8_t RTCZeroWrapper::getAlarmYear(uint8_t y)
{
	return rtc.getAlarmYear();
}

uint32_t RTCZeroWrapper::getEpoch()
{
	return rtc.getEpoch();
}

uint32_t RTCZeroWrapper::getY2kEpoch()
{
	return rtc.getY2kEpoch();
}

void RTCZeroWrapper::setEpoch(uint32_t ts)
{
	rtc.setEpoch(ts);
}

void RTCZeroWrapper::setY2kEpoch(uint32_t ts)
{
	rtc.setY2kEpoch(ts);
}

void RTCZeroWrapper::setAlarmEpoch(uint32_t ts)
{
	rtc.setAlarmEpoch(ts);
}

void RTCZeroWrapper::setSeconds(uint8_t seconds)
{
	rtc.setSeconds(seconds);
}

void RTCZeroWrapper::setMinutes(uint8_t minutes)
{
	rtc.setMinutes(minutes);
}

void RTCZeroWrapper::setHours(uint8_t hours)
{
	rtc.setHours(hours);
}

void RTCZeroWrapper::setTime(uint8_t hours, uint8_t minutes, uint8_t seconds)
{
	rtc.setTime(hours, minutes, seconds);
}

void RTCZeroWrapper::setDay(uint8_t day)
{
	rtc.setDay(day);
}

void RTCZeroWrapper::setMonth(uint8_t month)
{
	rtc.setMonth(month);
}

void RTCZeroWrapper::setYear(uint8_t year)
{
	rtc.setYear(year);
}

void RTCZeroWrapper::setDate(uint8_t day, uint8_t month, uint8_t year)
{
	rtc.setDate(day, month, year);
}

uint8_t RTCZeroWrapper::getDay()
{
	return rtc.getDay();
}

uint8_t RTCZeroWrapper::getMonth()
{
	return rtc.getMonth();
}

uint8_t RTCZeroWrapper::getYear()
{
	return rtc.getYear();
}

void RTCZeroWrapper::setAlarmTime(uint8_t h, uint8_t m, uint8_t s)
{
	rtc.setAlarmTime(h, m, s);
}

void RTCZeroWrapper::setAlarmDate(uint8_t d, uint8_t m, uint8_t y)
{
	rtc.setAlarmDate(d, m, y);
}

void RTCZeroWrapper::attachInterrupt(voidFuncPtr callback)
{
	rtc.attachInterrupt(callback);
}

void RTCZeroWrapper::detachInterrupt()
{
	rtc.detachInterrupt();
}
