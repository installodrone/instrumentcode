#pragma once

#include <LinkedList/LinkedList.h>
#include <cstdint>
#include "RTCWrapper.h"
#include <time.h>

class RTCTimeManager
{
public:
	enum RTCMask : uint8_t
	{
		MATCH_OFF,          // Never
		MATCH_SS,           // Every Minute
		MATCH_MMSS,         // Every Hour
		MATCH_HHMMSS,       // Every Day
		MATCH_DDHHMMSS,     // Every Month
		MATCH_MMDDHHMMSS,   // Every Year
		MATCH_YYMMDDHHMMSS  // Once, on a specific date and a specific time
	};
	class RTCTime {
	protected:

		uint8_t _seconds;
		uint8_t _minutes;
		uint8_t _hours;
		uint8_t _day;
		uint8_t _month;
		uint8_t _year;
	public :
		RTCTime();
		RTCTime(uint8_t s, uint8_t min, uint8_t h, uint8_t d, uint8_t mon, uint8_t y);

		bool isBefore(RTCTime* t);
		void toDateArray(uint8_t * arrayYMDHMS);

		inline uint8_t seconds() { return _seconds; };
		inline uint8_t minutes() { return _minutes; };
		inline uint8_t hours() { return _hours; };
		inline uint8_t day() { return _day; };
		inline uint8_t month() { return _month; };
		inline uint8_t year() { return _year; };

		inline void setSeconds(uint8_t a) { _seconds = a; };
		inline void setMinutes(uint8_t a) { _minutes = a; };
		inline void setHours(uint8_t a) { _hours = a; };
		inline void setDay(uint8_t a) { _day = a; };
		inline void setMonth(uint8_t a) { _month = a; };
		inline void setYear(uint8_t a) { _year = a; };
		long toEpoch();
		void toEpochString(char * buf);
		void setTime(RTCTime t) {
			_year = t._year;
			_month = t._month;
			_day = t._day;
			_hours = t._hours;
			_minutes = t._minutes;
			_seconds = t._seconds;
		}
	};
	class RTCAlarm : public RTCTime {
			
			RTCTimeManager* _mngr;
			voidFuncPtr _callback;

			RTCMask _mask;
			bool _repeat;

		public:
			RTCAlarm(RTCTimeManager* manager);
			RTCAlarm(RTCTimeManager* manager, uint8_t s, uint8_t min, uint8_t h, uint8_t d, uint8_t mon, uint8_t y);
			RTCAlarm(RTCTimeManager* manager, voidFuncPtr callback, RTCMask mask, uint8_t s, uint8_t min, uint8_t h, uint8_t d, uint8_t mon, uint8_t y);
			
			void alarmHandler();

			RTCTime nextAlarmTime();
			inline void setRepeat(bool r) { _repeat = r; };
			inline uint8_t mask() { return _mask; };
			inline bool repeat() { return _repeat; };
			inline void setMask(RTCMask a) { _mask = a; };
			inline void setCallback(voidFuncPtr a) { _callback = a; };
	};
	RTCTimeManager(voidFuncPtr f) : _rtc(nullptr), _handler(f) {};
	RTCTimeManager(voidFuncPtr f, RTCWrapper* r) : _rtc(r), _handler(f) {};
	void setRTC(RTCWrapper* r) { _rtc = r; };
	RTCTime now();
	void scheduleNextAlarm();
	RTCAlarm* getNext();
	void onAlarm();
	void createAlarm(voidFuncPtr callback, RTCMask mask, uint8_t seconds, uint8_t minutes = 0, uint8_t hours = 0, uint8_t day = 0, uint8_t month = 0, uint8_t year = 0);
	void renewAlarm();
	void placeInList(RTCTimeManager::RTCAlarm * alarm);
	

private:
	voidFuncPtr _handler;
	RTCWrapper* _rtc;
	LinkedList<RTCAlarm*> _alarms;
};