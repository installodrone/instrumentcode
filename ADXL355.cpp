#include "ADXL355.h"

ADXL355::ADXL355(uint8_t cs) : _cs(cs)
{
}

void ADXL355::init()
{
	pinMode(_cs, OUTPUT);
	digitalWrite(_cs, HIGH);
	SPI.begin();
	wake();
}

void ADXL355::wake()
{
	//turn on module
	uint8_t temp;
	temp = (uint8_t)readRegister(ADXL355_POWER);
	temp = temp & 0xFE;
	writeRegister(ADXL355_POWER, temp);
}
void ADXL355::standby()
{
	//turn off module
	uint8_t temp;
	temp = (uint8_t)readRegister(ADXL355_POWER);
	temp = temp | 0x01;
	writeRegister(ADXL355_POWER, temp);
}

int32_t ADXL355::readX()
{
	return (int32_t)dataConversion(readRegister(ADXL355_X, 3));
}

int32_t ADXL355::readY()
{
	return (int32_t)dataConversion(readRegister(ADXL355_Y, 3));
}

int32_t ADXL355::readZ()
{
	return (int32_t)dataConversion(readRegister(ADXL355_Z, 3));
}
uint32_t ADXL355::readRegister(byte lowReg, uint8_t nbBytes) {
	byte answer = 0;        // incoming byte from the SPI
	uint32_t result = 0;		// result to return


	digitalWrite(_cs, LOW);
	SPI.beginTransaction(SPISettings()); //default settings are ok !

	for (int i = 0; i < nbBytes; i++)
	{
		lowReg += i;
		// ADXL355 expects the register name in the upper 7 bits
		// of the byte
		lowReg = lowReg << 1;

		//read/_write_ bit addition
		byte address = lowReg | 0x01;

		SPI.transfer(address);
		answer = SPI.transfer(0x00);
		result = result << 8;
		result = result | answer;
	}
	SPI.endTransaction();
	digitalWrite(_cs, HIGH);
	
	return result;
}

void ADXL355::writeRegister(byte reg, byte value) {

	// ADXL355 expects the register name in the upper 7 bits
	// of the byte
	reg = reg << 1;
	
	//read/_write_ bit addition
	byte address = reg & 0xFE;

	digitalWrite(_cs, LOW);
	SPI.transfer(address);
	SPI.transfer(value);
	digitalWrite(_cs, HIGH);
}
int32_t ADXL355::dataConversion(uint32_t ui32SensorData)
{
	int32_t volatile i32Conversion = 0;

	ui32SensorData = (ui32SensorData >> 4);
	ui32SensorData = (ui32SensorData & 0x000FFFFF);

	if ((ui32SensorData & 0x00080000) == 0x00080000) {

		i32Conversion = (ui32SensorData | 0xFFF00000);

	}
	else {
		i32Conversion = ui32SensorData;
	}

	return i32Conversion;
}