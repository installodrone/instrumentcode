#include "HMSTime.h"

RTCTimeManager::RTCTime::RTCTime() 
	: _seconds(0), _minutes(0), _hours(0), _day(0), _month(0), _year(0){}
RTCTimeManager::RTCTime::RTCTime(uint8_t s, uint8_t min, uint8_t h, uint8_t d, uint8_t mon, uint8_t y) 
	: _seconds(s), _minutes(min), _hours(h), _day(d), _month(mon), _year(y){}
RTCTimeManager::RTCAlarm::RTCAlarm(RTCTimeManager* manager)
	: RTCTime(), _mngr(manager), _callback(nullptr), _mask(MATCH_YYMMDDHHMMSS), _repeat(false) {}
RTCTimeManager::RTCAlarm::RTCAlarm(RTCTimeManager* manager, uint8_t s, uint8_t min, uint8_t h, uint8_t d, uint8_t mon, uint8_t y)
	:  RTCTime(s,min,h,d,mon,y), _mngr(manager), _callback(nullptr), _mask(MATCH_YYMMDDHHMMSS), _repeat(false) {}
RTCTimeManager::RTCAlarm::RTCAlarm(RTCTimeManager* manager, voidFuncPtr callback, RTCMask mask, uint8_t s, uint8_t min, uint8_t h, uint8_t d, uint8_t mon, uint8_t y)
	: RTCTime(s, min, h, d, mon, y), _mngr(manager), _callback(callback), _mask(mask), _repeat(false) {}

bool RTCTimeManager::RTCTime::isBefore(RTCTime * t)
{
	uint8_t a1[6], a2[6]; 
	this->toDateArray(a1);
	t->toDateArray(a2);
	for (int i = 0; i < 6; i++)
	{
		if (a1[i] > a2[i]) return false;
		else if (a1[i] < a2[i]) return true;
	}
	//when equal
	return true;
}
void RTCTimeManager::RTCTime::toDateArray(uint8_t* arrayYMDHMS)
{
	arrayYMDHMS[0] = this->_year;
	arrayYMDHMS[1] = this->_month;
	arrayYMDHMS[2] = this->_day;
	arrayYMDHMS[3] = this->_hours;
	arrayYMDHMS[4] = this->_minutes;
	arrayYMDHMS[5] = this->_seconds;
}
inline void RTCTimeManager::RTCAlarm::alarmHandler()
{
	//call user's callback function, then schedule next alarm on RTC.
	if(_callback) _callback();
}

RTCTimeManager::RTCTime RTCTimeManager::now()
{
	if (_rtc) return RTCTime(_rtc->getSeconds(), _rtc->getMinutes(), _rtc->getHours(), _rtc->getDay(), _rtc->getMonth(), _rtc->getYear());
	else return RTCTime();
}

void RTCTimeManager::scheduleNextAlarm()
{
	RTCAlarm* nearest = getNext();
	
	_rtc->disableAlarm();
	_rtc->detachInterrupt();
	_rtc->setAlarmTime(nearest->seconds(), nearest->minutes(), nearest->hours());
	_rtc->setAlarmDate(nearest->day(), nearest->month(), nearest->year());
	_rtc->attachInterrupt(_handler);
	_rtc->enableAlarm(nearest->mask());
}

RTCTimeManager::RTCTime RTCTimeManager::RTCAlarm::nextAlarmTime()
{
	RTCTime t;
	switch (_mask)
	{
		//Careful when you read this : there is no breaks and it's ok !
		case MATCH_YYMMDDHHMMSS: 
			t.setYear(year());
		case MATCH_MMDDHHMMSS: 
			t.setMonth(month());
		case MATCH_DDHHMMSS: 
			t.setDay(day());
		case MATCH_HHMMSS: 
			t.setHours(seconds());
		case MATCH_MMSS: 
			t.setMinutes(minutes());
		case MATCH_SS: 
			t.setSeconds(seconds());
		case MATCH_OFF: 
			break;
	}
	return t;
}

inline RTCTimeManager::RTCAlarm* RTCTimeManager::getNext()
{
	//list is sorted when new alarm added, no need to search :)
	return _alarms.get(0);
}

void RTCTimeManager::onAlarm()
{
	RTCAlarm* alarm = getNext();
	//concerned alarm is always the first one 
	alarm->alarmHandler();
	
	//test if alarm is used later and clean
	if (!alarm->repeat())
		delete alarm;
	else
		renewAlarm();

	scheduleNextAlarm();
}

void RTCTimeManager::createAlarm(voidFuncPtr callback, RTCMask mask, uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day, uint8_t month, uint8_t year)
{
	RTCAlarm* toInsert = new RTCAlarm(this, callback, mask, seconds, minutes, hours, day, month, year);

	placeInList(toInsert);
}
void RTCTimeManager::renewAlarm()
{
	//remove alarm to renew
	RTCAlarm* alarm = _alarms.pop();
	
	//update time if necessary (user handler can modify alarm time)
	alarm->setTime(alarm->nextAlarmTime());
	
	//insert alarm renewed
	placeInList(alarm);
}
void RTCTimeManager::placeInList(RTCTimeManager::RTCAlarm* alarm)
{
	//find new place in the list
	int newplace = 0;
	if (_alarms.size() > 0)
	{
		for (int i = 0; i < _alarms.size(); i++)
		{
			if (alarm->isBefore(_alarms.get(i)))
			{
				_alarms.add(i, alarm);
				newplace = i;
				break;
			}
		}
	}
	else _alarms.add(alarm);

	//new alarm is also the next alarm to ring, so we must enable it and disable old one
	if (newplace == 0)
	{
		scheduleNextAlarm();
	}
}

long RTCTimeManager::RTCTime::toEpoch()
{
	struct tm toConvert;
	toConvert.tm_year = (2000 + _year)-1900;
	toConvert.tm_mon = _month-1;
	toConvert.tm_mday = _day;
	toConvert.tm_hour = _hours;
	toConvert.tm_min = _minutes;
	toConvert.tm_sec = _seconds;
	return mktime(&toConvert);
}
