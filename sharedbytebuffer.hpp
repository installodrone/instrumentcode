#pragma once
#include "sharedbuffer.hpp"

/**
 * @brief Shared buffer of bytes.
 */
using SharedByteBuffer = SharedBuffer<uint8_t>;

