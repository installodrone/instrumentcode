#pragma once


#define ADXL355_STATUS 0x04
#define ADXL355_FIFO_ENTRIES 0x05

#define ADXL355_X1	0x0A
#define ADXL355_X2	0x09
#define ADXL355_X3	0x08
#define ADXL355_X	ADXL355_X3

#define ADXL355_Y1	0x0D
#define ADXL355_Y2	0x0C
#define ADXL355_Y3	0x0B
#define ADXL355_Y	ADXL355_Y3

#define ADXL355_Z1	0x10
#define ADXL355_Z2	0x0F
#define ADXL355_Z3	0x0E
#define ADXL355_Z	ADXL355_Z3

#define ADXL355_POWER	0x2D

#include<SPI.h>
#include <ArduinoSTL.h>
class ADXL355
{
private:
	uint8_t _cs;
public:
	ADXL355(uint8_t cs);
	void init();
	void wake();
	void standby();
	int32_t readX();
	int32_t readY();
	int32_t readZ();
	uint32_t readRegister(byte lowReg, uint8_t nbBytes = 1);
	void writeRegister(byte reg, byte value);
	int32_t dataConversion(uint32_t ui32SensorData);
};

