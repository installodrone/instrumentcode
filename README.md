
# InstalloDrone
InstalloDrone is a Bachelor's grade project created by Loïc Gillioz with the supervision of Joseph Moerschell, in the University of Applied Science in Sion, Valais, Switzerland. 
## Goal
This project is the following of a previous project called ExtensoDrone, realised earlier by Loïc Gillioz and 4 other students. The main goal was to improve features of the previous project.

These 2 projects had to find a way to install some device to a cliff or some walls, using a drone.
## Content
This repository contains code for the remote of the installed device