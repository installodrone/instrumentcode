#include <SD.h>
#include <Arduino.h>
#include <ArduinoSTL.h>
#include <cstdint>
#include <ArduinoJson.h>
#include <Adafruit_BME680.h>

#include <RadioHead/RadioHead.h>
#include "rfm96wrapper.h"
#include "ADXL355.h"
#include "sharedbytebuffer.hpp"
#include "RTCZeroWrapper.hpp"
#include "HMSTime.h"

RTCWrapper* rtc;
RTCTimeManager* timeManager;

Adafruit_BME680* bme680;
ADXL355* adxl355;

RemoteToDrone* toRelay;
SharedByteBuffer frame(6);
File logfile;

char filename[11] = "LOG000.TXT";

uint32_t lastPos = 0;
#define SD_CS 10
#define RFM_CS 8
#define ADXL_CS 11
#define VBATPIN A7

#define MAX_MEASUREMENT_LENGTH 200

bool on = false;

void alarmHandler()
{
	timeManager->onAlarm();
}

void setup()
{
	pinMode(13, OUTPUT);
	//by default, disable all SPI modules (as I don't know foreign librairies init behavior)
	Serial.begin(115200);
	pinMode(RFM_CS, OUTPUT);	//RFM
	digitalWrite(RFM_CS, HIGH);
	pinMode(SD_CS, OUTPUT);		//SD
	digitalWrite(SD_CS, HIGH);
	pinMode(ADXL_CS, OUTPUT);	//ADXL
	digitalWrite(ADXL_CS, HIGH);	


	//Factory
	bme680 = new Adafruit_BME680();
	pinMode(13, HIGH);
	while (!bme680->begin()) {
		Serial.println("Could not find a valid BME680 sensor, check wiring!");
		while (1);
	}

	// Set up oversampling and filter initialization
	bme680->setTemperatureOversampling(BME680_OS_8X);
	bme680->setHumidityOversampling(BME680_OS_2X);
	bme680->setPressureOversampling(BME680_OS_4X);
	bme680->setIIRFilterSize(BME680_FILTER_SIZE_3);
	bme680->setGasHeater(320, 150); // 320*C for 150 ms

	adxl355 = new ADXL355(ADXL_CS);
	adxl355->init();
	byte analogDeviceID = adxl355->readRegister(0x00);
	Serial.println(analogDeviceID);

	rtc = new RTCZeroWrapper();
	//GMT (1-2 hours less than here in Sion)
	rtc->init(11, 51, 00, 25, 07, 18);
	timeManager = new RTCTimeManager(alarmHandler);
	timeManager->setRTC(rtc);

	
	toRelay = new RFM96Wrapper();
	toRelay->init();

	//timeManager->createAlarm(measure, HMSTimeManager::MATCH_SS, 10);
	//timeManager->createAlarm(loraSend, HMSTimeManager::MATCH_SS, 20);


	// see if the card is present and can be initialized:
	if (!SD.begin(SD_CS)) {
		Serial.println("Card init failed!");
	}
	getLastLogFile();
}
void getLastLogFile() {
	uint16_t i = 0;
	//find last created file
	while (SD.exists(filename))
	{
		i++;
		Serial.print("File ");
		Serial.print(filename);
		Serial.println(" exists.");
		sprintf(filename, "LOG%03d.txt", i);
	}
	i--;
	sprintf(filename, "LOG%03d.txt", i);
	Serial.print("Check ");
	Serial.println(filename);
	//verify it is not already full
	if (logfile = SD.open(filename, FILE_WRITE))
	{
		Serial.print("Size : ");
		Serial.println(logfile.size());
		if (logfile.size() >= UINT32_MAX - MAX_MEASUREMENT_LENGTH)
		{
			Serial.print("Not enough space, creating new file");
			i++;
			sprintf(filename, "LOG%03d.txt", i);
			logfile.close();
			logfile = SD.open(filename, FILE_WRITE);
		}
	}
	//now filename should be ok, check it
	if (logfile)
	{
		Serial.print("Successfully opened ");
		Serial.println(filename);
		//get last position
		lastPos = logfile.size();
	}
	else
	{
		Serial.println("Error with SD card files !");
		while (1);
	}
}

void loop()
{
	measure();
	digitalWrite(13, HIGH);
	delay(1000);
	loraSend();
	digitalWrite(13, LOW);
	delay(1000);

	//rtc->standbyMode(); //Sleep until next alarm
}

void measure() // Do something when interrupt called
{
	StaticJsonBuffer<MAX_MEASUREMENT_LENGTH> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();
	char timestamp[20];
	sprintf(timestamp, "%lu000000000", timeManager->now().toEpoch());
	root["time"] = timestamp;
	root["battery"] = analogRead(VBATPIN)*2.0*3.3/1024.0;
	root["temperature"] = bme680->readTemperature();
	root["humidity"] = bme680->readHumidity();
	root["pressure"] = bme680->readPressure()/100.0;
	root["gas"] = bme680->readGas()/1000.0;
	root["x"] = adxl355->readX();
	root["y"] = adxl355->readY();
	root["z"] = adxl355->readZ();


	logfile = SD.open(filename, FILE_WRITE);
	if (logfile) {

		// Serialize JSON to file
		if (root.printTo(logfile) == 0) {
			Serial.println(F("Failed to write to file"));
		}
		logfile.println();
		// Close the file (File's destructor doesn't close the file)
		logfile.close();
	}
}
void loraSend()
{
	//TODO : JSON ENCODING
	uint8_t buffer[MAX_MEASUREMENT_LENGTH];
	uint8_t len = 0;
	logfile = SD.open(filename, FILE_READ);
	if (logfile)
	{
		logfile.seek(lastPos);
		//send all we can
		while (logfile.position() < logfile.size())
		{
			//there is only 1 object per measurement, so we can use closing bracket as end of JSON measurement detection
			len = logfile.readBytesUntil('\n', buffer, MAX_MEASUREMENT_LENGTH);
			Serial.print("->");
			Serial.println((char*)buffer);
			if(toRelay->send(buffer, len)) Serial.println("Successfully sent");
		}
		lastPos = logfile.position();
		logfile.close();
	}

}

