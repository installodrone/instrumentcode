#include "rfm96wrapper.h"
#include <cstring>

RFM96Wrapper::RFM96Wrapper() :
	RemoteToDrone(),
	_theDriver(RFM96_CS, RFM96_INT),
	packetNumber(0)
{
	ptLength = new uint8_t; 
	ptTimeout = new uint8_t; 
	*ptLength = FRAME_LENGTH;
	*ptTimeout = TIMEOUT; 
}

RFM96Wrapper::~RFM96Wrapper()
{	
	delete ptLength; 
	delete ptTimeout; 
}

bool RFM96Wrapper::send(uint8_t* data, uint8_t length)
{
	return _theDriver.send(data, length);
}

void RFM96Wrapper::receive(uint8_t* data)
{
	if(_theDriver.available())
	{
		// Should be a message for us now
		uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
		uint8_t len = sizeof(buf);

		if (_theDriver.recv(data, ptLength))
		{
			digitalWrite(13, HIGH);
			RH_RF95::printBuffer("Received: ", buf, len);
			Serial.print("Got: ");
			Serial.println((char*)buf);
			Serial.print("RSSI: ");
			Serial.println(_theDriver.lastRssi(), DEC);
		}
		else
		{
			Serial.println("Receive failed");
		}
	}
}

void RFM96Wrapper::init()
{
	pinMode(RFM96_RST, OUTPUT);

	Serial.println("Feather LoRa TX Test!");

	// manual reset
	digitalWrite(RFM96_RST, LOW);
	delay(10);
	digitalWrite(RFM96_RST, HIGH);
	delay(10);

	while (!_theDriver.init()) {
		Serial.println("LoRa radio init failed");
		while (1);
	}
	Serial.println("LoRa radio init OK!");

	if (!_theDriver.setFrequency(RFM96_FREQ)) {
		Serial.println("setFrequency failed");
		while (1);
	}
	Serial.print("Set Freq to: "); Serial.println(RFM96_FREQ);

	_theDriver.setTxPower(13, false);
}