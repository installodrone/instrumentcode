#ifndef REMOTETODRONE_H
#define REMOTETODRONE_H
#include <cstdint>
/**
 * @brief The RemoteToDrone class is an interface forcing methods needed to communicate with the drone
 */
class RemoteToDrone
{
public:
    RemoteToDrone();
	virtual ~RemoteToDrone(); 
    virtual bool send(uint8_t* data, uint8_t length) = 0;
	virtual void receive(uint8_t* data) = 0; 
	virtual void init() = 0; 
};

#endif // REMOTETODRONE_H
