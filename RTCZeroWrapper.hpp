#pragma once

#include <RTCZero.h>
#include "RTCWrapper.h"
class RTCZeroWrapper : public RTCWrapper {
public :
	enum Mask : uint8_t
	{
		MATCH_OFF = RTCZero::MATCH_OFF,          // Never
		MATCH_SS = RTCZero::MATCH_SS,           // Every Minute
		MATCH_MMSS = RTCZero::MATCH_MMSS,         // Every Hour
		MATCH_HHMMSS = RTCZero::MATCH_HHMMSS,       // Every Day
		MATCH_DDHHMMSS = RTCZero::MATCH_DDHHMMSS,     // Every Month
		MATCH_MMDDHHMMSS = RTCZero::MATCH_MMDDHHMMSS,   // Every Year
		MATCH_YYMMDDHHMMSS = RTCZero::MATCH_YYMMDDHHMMSS  // Every Century :)
	};
	virtual void standbyMode();
	virtual void init(uint8_t h, uint8_t mi, uint8_t s, uint8_t d, uint8_t mo, uint8_t y) override;
private:
	RTCZero rtc;

	// H�rit� via RTCWrapper

	virtual void enableAlarm(uint8_t mask) override;
	virtual void disableAlarm() override;

	virtual void setSeconds(uint8_t seconds) override;
	virtual void setMinutes(uint8_t minutes) override;
	virtual void setHours(uint8_t hours) override;
	virtual void setTime(uint8_t hours, uint8_t minutes, uint8_t seconds) override;
	virtual void setDay(uint8_t day) override;
	virtual void setMonth(uint8_t month) override;
	virtual void setYear(uint8_t year) override;
	virtual void setDate(uint8_t day, uint8_t month, uint8_t year) override;
	virtual void setEpoch(uint32_t ts) override;
	virtual void setY2kEpoch(uint32_t ts) override;

	virtual uint8_t getSeconds() override;
	virtual uint8_t getMinutes() override;
	virtual uint8_t getHours() override;
	virtual uint8_t getDay() override;
	virtual uint8_t getMonth() override;
	virtual uint8_t getYear() override;
	virtual uint32_t getEpoch() override;
	virtual uint32_t getY2kEpoch() override;

	virtual void setAlarmSeconds(uint8_t s) override;
	virtual void setAlarmMinutes(uint8_t m) override;
	virtual void setAlarmHours(uint8_t h) override;
	virtual void setAlarmTime(uint8_t h, uint8_t m, uint8_t s) override;
	virtual void setAlarmDay(uint8_t d) override;
	virtual void setAlarmMonth(uint8_t m) override;
	virtual void setAlarmYear(uint8_t y) override;
	virtual void setAlarmDate(uint8_t d, uint8_t m, uint8_t y) override;
	virtual void setAlarmEpoch(uint32_t ts) override;

	virtual void attachInterrupt(voidFuncPtr callback) override;
	virtual void detachInterrupt() override;

	virtual uint8_t getAlarmSeconds(uint8_t s) override;
	virtual uint8_t getAlarmMinutes(uint8_t m) override;
	virtual uint8_t getAlarmHours(uint8_t h) override;
	virtual uint8_t getAlarmDay(uint8_t d) override;
	virtual uint8_t getAlarmMonth(uint8_t m) override;
	virtual uint8_t getAlarmYear(uint8_t y) override;

};