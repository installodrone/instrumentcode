#pragma once
#undef min
#undef max
#include <cstdint>
#include <memory>
#include <cstring>


/**
 * @brief Buffer for shared data allowing multiple instances to use actually the same buffer and releases the buffer once the last user drops it.
 *        The buffer is **not resizeable!**
 *
 * The buffer can be either proxy an existing buffer or a new buffer where the data is actually allocated on the heap can be created. If the
 * buffer is allocated dynamically on the heap, the heap resources will be released once the last holder of the buffer deletes the buffer object
 * automatically. The data is shared, so if someone makes changes on a buffer object, all other objects refering the same buffer do reflect these
 * changes. If you want your own copy of a buffer, you have to call the copy() method and use the SharedBuffer instance returned by that method.
 */
template <typename T>
class SharedBuffer {
public:

    SharedBuffer() {}
    /**
     * @brief Allocates and returns a shared buffer with the given size on the heap.
     *
     * @param length    The number of elements the buffer has to be in size.
     */
    explicit SharedBuffer(int length) {
        data_ = new T[length];
        length_ = length;
		clearBuffer(); 
    }

    /**
     * @brief Creates a shared buffer that represents the memory region passed by data and length.
     *
     * Note that it is important to understand that as long as there are instances of this buffer left, the memory at data
     * can be modified, so if you are passing a shared pointer to another method, check that the use count of the buffer is
     * 1 after the method was called in order to avoid dangling shared buffers still pointing to the memory.
     *
     * @param data      Pointer to the start of the memory region to represent by the shared buffer.
     * @param length    The actual number of elements inside the buffer.
     */
    static SharedBuffer proxy(T* data, int length) {
        SharedBuffer buffer;
        buffer.data_ = data;
        buffer.length_ = length;
        return buffer;
    }

    /**
     * @brief Creates a shared buffer by copying the data from the given memory location.
     *
     * Note that the actual buffer needed to hold the data is allocated on the heap.
     *
     * @param data      Pointer to the start of the readonly memory region to copy into the shared buffer.
     * @param length    The actual size of the memory region to copy into the buffer.
     */
    static SharedBuffer copy(T* data, int length) {
        SharedBuffer buffer;
        buffer.data_ = new T[length];
        buffer.length_ = length;
        std::memcpy(buffer.data_, data, length * sizeof(T));
        return buffer;
    }

    /**
     * @brief Returns a copy of himself by copying the data to the new shared buffer.
     *
     * Note that the actual buffer needed to hold the data is allocated on the heap.
     *
     * @return A new shared buffer as a copy.
     */
    SharedBuffer copy() const {
        SharedBuffer buffer;
        buffer.data_ = new T[length];
        buffer.length_ = length;
        std::memcpy(buffer.data_, data, length * sizeof(T));
        return buffer;
    }

    /**
     * @brief Returns the number of elements in the container, i.e. std::distance(begin(), end()).
     *
     * @return The number of elements in the container.
     */
    int size() const {
        return length_;
    }

    /**
     * @brief Returns the number of elements in the container, i.e. std::distance(begin(), end()).
     *
     * @return The number of elements in the container.
     */
    int length() const {
        return length_;
    }

    /**
     * @brief Checks if the container has no elements, i.e. whether begin() == end().
     *
     * @return True if the container is empty, false otherwise.
     */
    bool empty() const {
        return !length_;
    }

    /**
     * @brief Returns a reference to the element at specified location pos. No bounds checking is performed.
     *
     * @param pos   Position of the element to return.
     * @return      Reference to the requested element.
     */
    T& operator [](int pos) {
        return data_[pos];
    }

    /**
     * @brief Returns a constant reference to the element at specified location pos. No bounds checking is performed.
     *
     * @param pos   Position of the element to return.
     * @return      Reference to the requested element.
     */
    const T& operator [](int pos) const {
        return data_[pos];
    }

    /**
     * @brief Returns pointer to the underlying array serving as element storage. The pointer is such that range
     *        [data(); data() + size()) is always a valid range, even if the container is empty.
     *
     * @return Pointer to the underlying element storage. For non-empty containers, returns &front().
     */
    T* data() {
        return data_;
    }

    /**
     * @brief Returns pointer to the underlying array serving as element storage. The pointer is such that range
     *        [data(); data() + size()) is always a valid range, even if the container is empty.
     *
     * @return Pointer to the underlying element storage. For non-empty containers, returns &front().
     */
    const T* data() const {
        return data_;
    }

    void clearBuffer()
    {
        for(int i=0; i < length_; i++)
        {
            data_[i] = 0;
        }
    }

    private:
        T* data_;
        int length_;
};



